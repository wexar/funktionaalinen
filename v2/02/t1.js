function laskePisteet(pituus, kPiste, lisapisteet){
    var pisteet = 60;
    
    if (pituus < kPiste)
        pisteet += (kPiste-pituus) *lisapisteet
    else if (pituus > kPiste)
        pisteet -= (kPiste-pituus) *lisapisteet
    
    return pisteet
    
}

function laskija(kPiste,lisapisteet) {
    return function (pituus) {
        return laskePisteet(pituus,kPiste,lisapisteet)
    }
}

const normaaliLahti = laskija(90,2)
const suurLahti = laskija(116,1.8)

console.log(normaaliLahti(91))
console.log(suurLahti(117))

