const Immutable = require('immutable');

const Auto  = (function(){
    
    const suojatut = new WeakMap();
    
    class Auto {
      constructor(tankki, matkamittari) {
        this.tankki = tankki;
        suojatut.set(this, {matkamittari: matkamittari});
        //this.matkamittari = matkamittari;
      }
      getTankki() {
        return this.tankki
      }
      getMatkamittari() {
          return suojatut.get(this).matkamittari
      }
      
      aja(auto) {
        console.log("ajetaan vähän matkaa")
        auto.tankki = auto.tankki - 10;
        suojatut.set(this, {matkamittari: this.getMatkamittari() + 1 }); 
}
    }
    return Auto;
})();



const auto1 = new Auto(100, 20);

auto1.aja(auto1)
console.log(auto1.getTankki())
console.log(auto1.getMatkamittari())

const auto2 = Immutable.Auto({a:1, b:2, c:3});
const auto = auto2.set('b', 50);