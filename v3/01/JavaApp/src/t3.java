import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;


public class t3 {

	public static void main(String[] args) {
		
		int count = 0;
		List<Integer> numbers =  new Random()
								.ints(1,7)
								.limit(20)
								.boxed()
								.collect(Collectors.toList());
								
		for (int i = 0; i < numbers.size(); i++) {
			if (numbers.get(i) == 6) {
				count++;
			}
			}
	
		System.out.println(numbers);
		System.out.println("there are " + count + " of number six");
		
			}				  


	}
