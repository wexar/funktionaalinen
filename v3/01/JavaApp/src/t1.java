package src

interface ArgInterface {

    public double operation(double a);
}

public class t1 {

    public static void main(String javalatte[]) {
        // this is lambda expression
        ArgInterface toCelcius = (a) -> (5/9) * (a-32);
        ArgInterface area = (a) -> Math.PI * a * a;
        
        System.out.println("10 fahrenheit is : " + toCelcius.operation(10));
        System.out.println("circle that has radius of 10 has area of : " + area.operation(10));

    }
}