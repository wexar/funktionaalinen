'use strict';

let f, g;
function foo() {
  let x;
  f = function() {  x++ };
  g = function() {  x-- };
  x = 1;
  console.log('inside foo, call to f(): ' + f());
}
foo();  

var Moduuli =(function() {
    let _x =0;
    return {
      kasvata: function () {
          return ++_x;
      },
      vahenna: function () {
          return --_x;
      }
};
})();

console.log(Moduuli.kasvata()); 
console.log(Moduuli.kasvata()); 
console.log(Moduuli.vahenna()); 
console.log(Moduuli.vahenna()); 
