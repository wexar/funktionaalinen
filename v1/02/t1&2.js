const f = function () {
    return function (x,y) {
        if (x < y) {
            return -1;
        }
        else if (x > y ) {
            return 1;
        }
        else {
            return 0;
        }
    }
}(); // Huomaa funktion kutsu!


let vertaa = f(1,2);
console.log(vertaa)


function compare(yourFunction, list1, list2) {
    let higher= 0;
    for (let i=0; i < list1.length; i++) {
        if(yourFunction(list1[i],list2[i]) === -1)
        higher++;
    }
    return higher;
}

console.log (compare(f, [1,2,3,-3,-6,12,12,12,-66,6,4,5],[20,-6,-8,12,2,36,7,1,7,4,-12,-21]));
