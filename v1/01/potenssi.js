var potenssi = function(base, exponent) {
  return exponent == 0? 1 : base * potenssi(base, --exponent);
};

console.log(potenssi(2,10))