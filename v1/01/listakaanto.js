var flippedList = []
var numbers =[1,2,3,4,5,6,7,8,9]

var flip = function(list) {

 if (list.length !== 0) {
    flippedList.push(list.pop())
    return flip(list)
 }
 else {
     return flippedList
 }
};

console.log(flip(numbers))